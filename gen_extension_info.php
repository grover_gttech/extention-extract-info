<?php
/**
 * Generate php files that contains each class and interface from an extension.
 * This could be useful like complementary project for autocomplete feature on 
 * some IDE that does not support  the extension.
 *
 * @author Grover Manuel Campos Ancajima <grover@gttech.pe>
 * @version 0.1
 * @copyright GT Tech <http://www.gttech.pe>
 * @license Apache 2.0
 * Copyright 2014 GT Tech E.I.R.L.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

# Get reflection over extension
$refExt = new ReflectionExtension('phalcon');

# Get the classes of extension
$classes = $refExt->getClasses();

# Lambda for get the name from namespace
$fncGetName = function ($full_name) {
    return substr($full_name, strrpos($full_name, '\\') + 1);
};

# iterate for classes
foreach ($classes as $classInfo) {
    
    # Parent class, if any
    $parent = $classInfo->getParentClass();
    
    # Interafaces
    $interfaces = $classInfo->getInterfaceNames();
    
    # Buffer for content
    $buffer = "<?php\n";
    
    if ($classInfo->inNamespace()) {
        $buffer .= 'namespace ' . $classInfo->getNamespaceName() . ';';
    }
    $buffer .= "\n\n";
    
    // Class signature
    $classLine = '';
    if (! $classInfo->isInterface() && $classInfo->isAbstract()) {
        $classLine .= 'abstract class ';
    } elseif ($classInfo->isFinal()) {
        $classLine .= 'final class ';
    } elseif ($classInfo->isInterface()) {
        $classLine .= 'interface ';
    } else {
        $classLine .= 'class ';
    }
    $classLine .= $fncGetName($classInfo->name);
    
    if (! empty($parent)) {
        $classLine .= ' extends \\' . $parent->name;
    }
    if (! empty($interfaces)) {
        $uses = array();
        $interfacesBag = array_map(function ($it) use ($fncGetName, &$uses) {
            $isMvcDisp = $it == 'Phalcon\Mvc\DispatcherInterface';
            $isRoot = strpos($it, '\\') === false;
            
            if (!$isRoot) {
                $uses[] = $isMvcDisp ? "$it as MvcDispatcher" : $it;
                $name = $isMvcDisp ? 'MvcDispatcher' : $fncGetName($it);
            } else {
                $name = "\\$it";
            }
            return $name;
        }, $interfaces);
        
        if (count($interfacesBag) > 1) {
            $classLine .= " implements\n    " . join(",\n    ", $interfacesBag);
        } else {
            $classLine .= ' implements '.current($interfacesBag);
        }
        foreach ($uses as $useIt) {
            $buffer .= "use $useIt;\n";
        }
        $buffer .= "\n";
    }
    $buffer .= "$classLine\n{\n\n";
    
    // Constants
    $constants = $classInfo->getConstants();
    if (! empty($constants)) {
        foreach ($constants as $constantName => $constantValue) {
            $buffer .= "\tconst $constantName = ";
            if (is_string($constantValue)) {
                $buffer .= "\"$constantValue\"";
            } else {
                $buffer .= $constantValue;
            }
            $buffer .= ";\n";
        }
    }
    
    // Methods
    $bitmask = ReflectionMethod::IS_FINAL | ReflectionMethod::IS_PUBLIC | ReflectionMethod::IS_STATIC;
    $methods = $classInfo->getMethods($bitmask);
    foreach ($methods as $methodInfo) {
        $classDec = $methodInfo->getDeclaringClass();
        if ($classInfo->name == $classDec->name && strpos($methodInfo->name, '_') !== 0) {
            
            $documentation = $methodInfo->getDocComment();
            if (! empty($documentation)) {
                $buffer .= "\n\n$documentation\n";
            }
            
            $soloPublic = true;
            if ($methodInfo->isAbstract() && ! $classInfo->isInterface()) {
                $buffer .= "    abstract ";
                $soloPublic = false;
            } elseif ($methodInfo->isFinal()) {
                $buffer .= "    final ";
                $soloPublic = false;
            }
            
            if ($soloPublic) {
                $buffer .= "    ";
            }
            $buffer .= "public ";
            if ($methodInfo->isStatic()) {
                $buffer .= "static ";
            }
            $bufferMethod = "function {$methodInfo->name}";
            
            # Parameters
            if ($methodInfo->getNumberOfParameters() > 0) {
                $parameters = $methodInfo->getParameters();
                $bufferParameters = array_map(function ($parameterInfo) use ($methodInfo) {
                    $bufferParameter = '';
                    if ($parameterInfo->isArray()) {
                        $bufferParameter .= "array ";
                    }
                    $bufferParameter .= '$' . $parameterInfo->name;
                    
                    # Default values
                    if ($parameterInfo->allowsNull()) {
                        $bufferParameter .= " = null";
                    } elseif ($parameterInfo->isOptional() && ! $methodInfo->isInternal()) {
                        $default = $parameterInfo->getDefaultValue();
                        if (is_string($default)) {
                            $default = '"' . $default . '"';
                        }
                        $bufferParameter .= "= " . $default;
                    }
                    ;
                    return $bufferParameter;
                }, $parameters);
                
                $fullParameters = join(', ', $bufferParameters);
                $lineNoFull = strlen("$bufferMethod$fullParameters") < 120;
                if ($lineNoFull) {
                    $bufferMethod .= '('.join(', ', $bufferParameters).(!$classInfo->isInterface() ? ")\n    {": ')');
                } else {
                    $aux = join(",\n        ", $bufferParameters);
                    $bufferMethod .= "(\n        $aux\n    )".(!$classInfo->isInterface() ? ' {' : '');
                }
                
            } elseif (!$classInfo->isInterface()) {
                $bufferMethod .= "()\n    {";
            } else {
                $bufferMethod .= "()";
            }
            
            
            if ($classInfo->isInterface()) {
                $bufferMethod .= ';';
            } else {
                $bufferMethod .= "\n    }";
            }
            $buffer .= "$bufferMethod\n";
        }
    }
    $buffer .= "}\n";
    
    # File name is the same of class name
    $filename = str_replace('\\', '_', $classInfo->name);
    
    # Put it on file
    file_put_contents("$filename.php", $buffer);
}
