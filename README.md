# Script for extract information about an PHP extension

This script was written by [GT Tech](http://www.gttech.pe "GT Tech") to extract the information from an extension through the PHP's Reflection API, with information obtained, php files that contain the definition of each class and interface found in the extension is generated. The files thus generated can be used in a complementary project at some ide like Eclipse PDT, so you can have the ide autocomplete feature.

More information at [this link](http://www.gttech.pe/soporte-autocompletado-de-phalcon-en-zend-studio/).
